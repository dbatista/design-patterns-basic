package edu.ccat.composite.interfaces;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 26/11/2016.
 */
public class FolderComposite implements FileComponent {

    private List<FileComponent> files;
    private String name;

    public FolderComposite(String name) {
        this.name = name;
        this.files = new ArrayList<FileComponent>();
    }

    public void addFileComponent(FileComponent file) {
        this.files.add(file);
    }

    public void print() {
        System.out.println("->" + this.name);
        for(FileComponent file : files) {
            file.print();
        }
    }
}
