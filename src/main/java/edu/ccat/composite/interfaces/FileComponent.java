package edu.ccat.composite.interfaces;

/**
 * Created by Luis Roberto Perez on 26/11/2016.
 */
public interface FileComponent {

    void print();
}
