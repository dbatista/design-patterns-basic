package edu.ccat.composite.interfaces;

/**
 * Created by Luis Roberto Perez on 26/11/2016.
 */
public class FileLeaf implements FileComponent {

    private String name;

    public FileLeaf(String name) {
        this.name = name;
    }

    public void print() {
        System.out.println(this.name);
    }
}
