package edu.ccat.composite.interfaces;

/**
 * Created by Luis Roberto Perez on 26/11/2016.
 */
public class Main {

    public static void main(String[] args) {
        FolderComposite root = new FolderComposite("C:/");
        root.addFileComponent( new FileLeaf("desktop.ini"));
        root.addFileComponent( new FileLeaf("some.ext"));

        FolderComposite programFiles = new FolderComposite("ProgramFiles/");

        FolderComposite skype = new FolderComposite("Skype/");
        skype.addFileComponent(new FileLeaf("Skype.exe"));
        skype.addFileComponent(new FileLeaf("user.conf"));

        programFiles.addFileComponent(skype);

        root.addFileComponent(programFiles);

        root.print();
    }
}
