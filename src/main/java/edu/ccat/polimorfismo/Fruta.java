package edu.ccat.polimorfismo;

import java.awt.*;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public abstract class Fruta {

    public abstract void dibujar();

    public void comer() {
        System.out.println("Estoy comiendo esta fruta");
    }
}
