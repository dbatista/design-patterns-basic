package edu.ccat.polimorfismo;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Transporte {

    private String brand;

    public Transporte(String brand) {
        this.brand = brand;
    }

    public void move() {
        // Se debe mover
        System.out.println(brand + " se mueve");
    }
}
