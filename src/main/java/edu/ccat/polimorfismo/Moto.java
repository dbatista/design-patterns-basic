package edu.ccat.polimorfismo;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Moto extends Transporte {
    public Moto(String brand) {
        super(brand);
    }
}
