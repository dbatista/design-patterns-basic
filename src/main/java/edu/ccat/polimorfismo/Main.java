package edu.ccat.polimorfismo;

import javax.swing.*;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Main {

    public static void main(String[] args) {
        Icon redBox = new BoxIcon();

        Transporte transporte = new Transporte("Cualquier");
        Auto auto = new Auto("Toyota");
        Auto2Wheels auto2 = new Auto2Wheels("Toyota");

        moverTransporte(transporte);
        moverTransporte(auto);
        moverTransporte(auto2);

        dibujarFruta(new Manzana());
        dibujarFruta(new Banana());

        Manzana manzana = new Manzana();
        manzana.comer();

        Banana banana = new Banana();
        banana.comer();
    }

    public static void moverTransporte(Transporte t) {
        t.move();
    }

    public static void dibujarFruta(Fruta fruta) {
        fruta.dibujar();
    }
}
