package edu.ccat.polimorfismo;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class BoxIcon implements Icon {
    public void paintIcon(Component c, Graphics pincel, int x, int y) {
        pincel.setColor(Color.RED);
        pincel.fillRect(0,0,50,50);

        pincel.setColor(Color.BLACK);
        pincel.fillRect(50,50,100,100);

    }

    public int getIconWidth() {
        return 150;
    }

    public int getIconHeight() {
        return 150;
    }
}
