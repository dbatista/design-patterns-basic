package edu.ccat.polimorfismo;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Auto extends Transporte {

    public Auto(String brand) {
        super(brand);
    }
}
