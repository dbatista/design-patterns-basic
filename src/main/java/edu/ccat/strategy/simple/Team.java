package edu.ccat.strategy.simple;

/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public class Team {

    private Strategy strategy;

    public Team(Strategy strategy) {
        this.strategy = strategy;
    }

    public void play() {
        this.strategy.play();
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

}
