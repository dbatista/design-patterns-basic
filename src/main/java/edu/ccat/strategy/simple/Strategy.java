package edu.ccat.strategy.simple;

/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public interface Strategy {

    // doWork() without parameters
    void play();
}
