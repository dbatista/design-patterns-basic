package edu.ccat.strategy.simple;

/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public class Attack implements Strategy {
    public void play() {
        System.out.println("attack the opponent");
    }
}
