package edu.ccat.strategy.simple;

import java.util.Random;

/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public class Game {

    public static void main(String[] args) {
        Attack attack = new Attack();
        Defense defense = new Defense();

        Team red = new Team(new Attack());
        Team blue = new Team(defense);

        //Rojo tiene el balon
        red.setStrategy(attack);
        red.play();
        //Blue tiene el balon
        red.setStrategy(defense);
        red.play();
        //Rojo tiene el balon
        red.setStrategy(attack);
        red.play();

        Random random = new Random();
        //si el numero es par entonces blue tiene el balon
        //si es impar blue no tiene balon
        for (int i=0; i<100; i++) {
            if(random.nextInt() % 2 == 0) {
                red.setStrategy(defense);
            } else {
                red.setStrategy(attack);
            }
            red.play();
        }
    }
}
