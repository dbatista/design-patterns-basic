package edu.ccat.strategy.kata;

/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public interface Strategy {
     void updateQuality(Item item);
}
