package edu.ccat.strategy.kata;

/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public class MixerQuality implements Strategy {
    public void updateQuality(Item item) {
        if(item.getSellIn() <= 10 && item.getSellIn() > 5){
            item.setQuality(item.getQuality()+2);
            return;
        }

        if(item.getSellIn() <= 5 && item.getSellIn() >0){
            item.setQuality(item.getQuality() +3);
            return;
        }

        if (item.getSellIn() == 0){
            item.setQuality(0);
            return;
        }




    }
}
