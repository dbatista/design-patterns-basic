package edu.ccat.strategy.parameters;

/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public class Attack implements Strategy {

    public void play(String name) {
        System.out.println("Attack with " + name);
    }
}
