package edu.ccat.strategy.parameters;


/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public class Team {

    private Strategy strategy;
    private String starPlayer;

    public Team(Strategy strategy) {
        this.strategy = strategy;
        this.starPlayer = "Messi";
    }

    public void play() {
        this.strategy.play(this.starPlayer);
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

}
