package edu.ccat.strategy.parameters;

/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public interface Strategy {

    void play(String nameAttacker);
}
