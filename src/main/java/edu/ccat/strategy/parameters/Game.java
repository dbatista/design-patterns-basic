package edu.ccat.strategy.parameters;


/**
 * Created by Luis Roberto Perez on 19/11/2016.
 */
public class Game {

    public static void main(String[] args) {
        Attack attack = new Attack();

        Team barza = new Team(attack);

        barza.play();
    }
}
