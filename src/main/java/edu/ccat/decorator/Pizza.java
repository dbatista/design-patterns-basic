package edu.ccat.decorator;

/**
 * Created by Luis Roberto Perez on 26/11/2016.
 */
public class Pizza implements Food {
    public String describe() {
        return "Classic Pizza";
    }

    public int getPrice() {
        return 15;
    }
}
