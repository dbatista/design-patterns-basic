package edu.ccat.decorator;

/**
 * Created by Luis Roberto Perez on 26/11/2016.
 */
public interface Food {

    String describe();
    int getPrice();
}
