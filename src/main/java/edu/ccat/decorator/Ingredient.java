package edu.ccat.decorator;

/**
 * Created by Luis Roberto Perez on 26/11/2016.
 */
public class Ingredient implements Food  {

    private Food pizza;
    private String name;
    private int price;

    public Ingredient(String name, int price, Food pizza) {
        this.name = name;
        this.price = price;
        this.pizza = pizza;
    }

    public String describe() {
        //utiliza al base
        return String.format("%s + %s", pizza.describe(), this.name);
    }

    public int getPrice() {
        return pizza.getPrice() + price;
    }
}
