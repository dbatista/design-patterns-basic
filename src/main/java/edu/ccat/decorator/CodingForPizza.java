package edu.ccat.decorator;

/**
 * Created by Luis Roberto Perez on 26/11/2016.
 */
public class CodingForPizza {

    public static void main(String[] args) {
        Food pizza = new Pizza();

        // Food tomatoe = new Ingredient("Tomatoe", pizza);
        // Food bound = new Ingredient("Borde", tomatoe);

        Food finalPizza = new
            Ingredient("Tomatoe", 2,
            new
                Ingredient("Borde", 15,
                new
                   Ingredient("Carne", 20,
                   new
                      Ingredient("Piña", 12,
                      new
                         Ingredient("Tocino", 8,
                         new
                            Ingredient("Choclo", 5,
                         pizza))))));

        System.out.println(finalPizza.describe() + " " + finalPizza.getPrice() + "Bs");
    }
}
