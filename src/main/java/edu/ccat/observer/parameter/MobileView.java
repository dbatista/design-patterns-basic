package edu.ccat.observer.parameter;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public class MobileView implements ViewObserver {
    public void update(int value) {
        System.out.println("I'm Mobile and I was notified with " + value);
    }
}
