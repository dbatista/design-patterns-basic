package edu.ccat.observer.parameter;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public class Parameter {
    public static void main(String[] args) {
        ModelSubject subject = new ModelSubject();

        ViewObserver web = new WebView();
        ViewObserver mobile = new MobileView();

        subject.attach(web);
        subject.attach(mobile);

        subject.setValue(8);

        subject.setValue(85);

        subject.setValue(75);
    }
}
