package edu.ccat.observer.parameter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public class ModelSubject {

    private int value;
    private final List<ViewObserver> observers;

    public ModelSubject() {
        this.value = 0;
        this.observers = new ArrayList<ViewObserver>();
    }

    public void attach(ViewObserver observer) {
        observers.add(observer);
    }

    private void notifyForUpdate() {
        // this.observers.forEach( o -> o.update() );
        for (ViewObserver observer : this.observers) {
            observer.update(this.value);
        }
    }

    public void setValue(int value) {
        this.value = value;
        this.notifyForUpdate();
    }
}
