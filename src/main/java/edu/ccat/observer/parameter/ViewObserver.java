package edu.ccat.observer.parameter;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */

// Observer
public interface ViewObserver {

    void update(int value);
}
