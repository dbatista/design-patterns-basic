package edu.ccat.observer.general;

import edu.ccat.observer.parameter.ViewObserver;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public interface Subject {

    void attach(ViewObserver observer);
    void notifyForUpdate();
}
