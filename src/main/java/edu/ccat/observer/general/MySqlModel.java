package edu.ccat.observer.general;

import edu.ccat.observer.parameter.ViewObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public class MySqlModel implements Subject {

    private String data;
    private List<ViewObserver> observers;

    public MySqlModel() {
        this.observers = new ArrayList<ViewObserver>();
    }

    public void setData(String data) {
        this.data = data;
    }

    public void attach(ViewObserver observer) {
        this.observers.add(observer);
    }

    public void notifyForUpdate() {
        for (ViewObserver observer : this.observers) {
            observer.update(5);
        }
    }
}
