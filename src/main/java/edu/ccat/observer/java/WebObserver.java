package edu.ccat.observer.java;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public class WebObserver implements Observer {
    public void update(Observable subject, Object arg) {
        System.out.println("The Subject changed " + (String)arg);
    }
}
