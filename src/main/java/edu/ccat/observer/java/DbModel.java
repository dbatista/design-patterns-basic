package edu.ccat.observer.java;

import java.util.Observable;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public class DbModel extends Observable {

    public void setSomething() {
        //changes

        this.setChanged();
        this.notifyObservers("Roberto");
    }

}
