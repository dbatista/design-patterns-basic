package edu.ccat.observer.java;

import java.util.Observer;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public class Main {
    public static void main(String[] args) {
        DbModel model = new DbModel();
        Observer web = new WebObserver();

        model.addObserver(web);

        model.setSomething();
    }
}
