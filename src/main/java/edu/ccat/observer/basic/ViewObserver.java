package edu.ccat.observer.basic;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */

// Observer
public interface ViewObserver {

    void update();
}
