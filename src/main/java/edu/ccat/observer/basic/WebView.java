package edu.ccat.observer.basic;

/**
 * Created by Luis Roberto Perez on 12/11/2016.
 */
public class WebView implements ViewObserver {
    public void update() {
        System.out.println("I'm Wwe Observer and I was notified");
    }
}
