package edu.ccat.singleton;

/**
 * Created by Luis Roberto Perez on 3/12/2016.
 */
public class Main {
    public static void main(String[] args) {
        ChocolateMachine machine = ChocolateMachine.getInstance();
        System.out.println(machine);

        ChocolateMachine machine2 = ChocolateMachine.getInstance();
        System.out.println(machine2);

        ChocolateMachine machine3 = ChocolateMachine.getInstance();
        System.out.println(machine3);

        ChocolateMachine woncka = ChocolateMachine.getInstance();
        System.out.println(woncka);
    }
}
