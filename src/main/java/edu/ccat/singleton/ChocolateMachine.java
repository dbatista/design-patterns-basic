package edu.ccat.singleton;

import java.util.Random;

/**
 * Created by Luis Roberto Perez on 3/12/2016.
 */
public class ChocolateMachine {
    private boolean empty;
    private boolean mixing;
    private final Random random = new Random();
    private int id;
    private static ChocolateMachine instance;

    private ChocolateMachine() {
        this.empty = true;
        this.mixing = false;
        id = random.nextInt();
    }

    public static ChocolateMachine getInstance() {
        if(instance == null) {
            synchronized (ChocolateMachine.class) {
                if(instance == null) {
                    instance = new ChocolateMachine();
                }
            }
        }
        return instance;
    }

    public void fill() {
        if( isEmpty() ) {
            empty = false;
        }
    }

    private boolean isEmpty() {
        return this.empty;
    }

    public void mix() {
        if( !isEmpty() && !isMixing() ) {
            this.mixing = true;
        }
    }

    private boolean isMixing() {
        return this.mixing;
    }

    public String toString() {
        return "ID: " + id;
    }
}
