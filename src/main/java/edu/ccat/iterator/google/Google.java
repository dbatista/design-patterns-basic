package edu.ccat.iterator.google;

import edu.ccat.iterator.Company;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Google implements Company {

    public GoogleUser[] getGoogleUsers() {
        //conecta a google y obtiene usuario
        GoogleUser[] users = new GoogleUser[3];
        users[0] = new GoogleUser("David", "david@gmail.com");
        users[1] = new GoogleUser("Guillermo", "guille@gmail.com");
        users[2] = new GoogleUser("Alexis", "alexis@gmail.com");

        return users;
    }

    public Iterator createIterator() {
        return Arrays.asList(getGoogleUsers()).iterator();
    }
}
