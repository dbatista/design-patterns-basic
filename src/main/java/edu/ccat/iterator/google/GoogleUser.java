package edu.ccat.iterator.google;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class GoogleUser {
    private String name;
    private String email;

    public GoogleUser(String name, String email) {
        this.name = name;
        this.email = email;
    }

    @Override
    public String toString() {
        // "GoogleUser: Luis Roberto <luis.robertop87@gmail.com>"
        return String.format("GoogleUser: %s <%s>", name, email);
    }
}
