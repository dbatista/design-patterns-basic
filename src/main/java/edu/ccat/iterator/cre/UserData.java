package edu.ccat.iterator.cre;

/**
 * Created by Luis Roberto Perez on 11/11/2016.
 */
public class UserData {
    private String name;
    private String address;
    private double payment;

    public UserData(String name, String address, double payment) {
        this.name = name;
        this.address = address;
        this.payment = payment;
    }

    @Override
    public String toString() {
        return String.format("CRE User: %s Address: %s Payment: %.2f", this.name, this.address, this.payment);
    }
}
