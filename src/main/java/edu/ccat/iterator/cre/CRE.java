package edu.ccat.iterator.cre;

import edu.ccat.iterator.Company;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Luis Roberto Perez on 11/11/2016.
 */
public class CRE implements Company {

    private Map<Integer, UserData> usersInformation;

    public CRE() {
        this.usersInformation = new HashMap<Integer, UserData>();
        this.usersInformation.put(1, new UserData("Roberto", "Guapilo 70", 350));
        this.usersInformation.put(2, new UserData("Pinta", "España 58", 856));
        this.usersInformation.put(3, new UserData("Niña", "Barcelona 78", 956));
        this.usersInformation.put(4, new UserData("Santa Maria", "America 458", 899));
    }

    public Map<Integer, UserData> getCREinfo() {
        return this.usersInformation;
    }

    public Iterator createIterator() {
        return getCREinfo().values().iterator();
    }
}
