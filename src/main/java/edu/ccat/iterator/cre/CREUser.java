package edu.ccat.iterator.cre;

/**
 * Created by Luis Roberto Perez on 11/11/2016.
 */
public class CREUser {
    private int id;
    private UserData userData;

    public CREUser(int id, UserData userData) {
        this.id = id;
        this.userData = userData;
    }
}
