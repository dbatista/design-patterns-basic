package edu.ccat.iterator;

import edu.ccat.iterator.cre.CRE;
import edu.ccat.iterator.facebook.Facebook;
import edu.ccat.iterator.google.Google;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class Main {

    public static void main(String[] args) {
        ArrayList<Company> companies = new ArrayList<Company>();
        companies.add(new Google());
        companies.add(new Facebook());
        companies.add(new CRE());

        for (Company company : companies) {
            showUsers(company.createIterator());
        }
    }

    public static void showUsers(Iterator iterator) {
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

}
