package edu.ccat.iterator;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public interface UsersIterator<T> {

    T next();
    boolean hasNext(); //isDone()
    T currentUser();
}
