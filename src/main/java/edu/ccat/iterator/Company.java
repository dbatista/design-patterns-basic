package edu.ccat.iterator;

import java.util.Iterator;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
// Este es el Agreggate
public interface Company {

    Iterator createIterator();
}
