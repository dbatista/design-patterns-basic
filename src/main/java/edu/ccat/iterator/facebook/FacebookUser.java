package edu.ccat.iterator.facebook;

/**
 * Created by Luis Roberto Perez on 5/11/2016.
 */
public class FacebookUser {
    private String fbName;
    private String fbUrl;

    public FacebookUser(String fbName, String fbUrl) {
        this.fbName = fbName;
        this.fbUrl = fbUrl;
    }

    @Override
    public String toString() {
        // FacebookUser: Shigeo T., url: https://facebook.com/user/shigeoLoveMe
        return String.format("FacebookUser: %s, url: %s", fbName, fbUrl);
    }
}
